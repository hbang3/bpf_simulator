package server

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	promRequestCount = promauto.NewCounter(prometheus.CounterOpts{
		Name: "bpf_simulator_request_total",
		Help: "total number of requests",
	})

	promRequestProcessTimeHist = promauto.NewHistogram(prometheus.HistogramOpts{
		Name: "bpf_simulator_request_process_time",
		Help: "time takes to process a request in milliseconds",
		Buckets: []float64{
			200.0,
			400.0,
			800.0,
			1000.0,
			1200.0,
			1400.0,
			1600.0,
		},
	})
)
