package server

import (
	"fmt"
	"net/http"
)

const MAX_PCAP_FILE_SIZE = 2 << 20

func (s *Server) render(w http.ResponseWriter, r *http.Request, status int, page string, data templateData) {
	ts, ok := s.templateCache[page]
	if !ok {
		err := fmt.Errorf("template %d does not exists", page)
		s.serverError(w, r, err)
		return
	}

	w.WriteHeader(status)

	err := ts.ExecuteTemplate(w, "base.tmpl.html", data)
	if err != nil {
		s.serverError(w, r, err)
	}
}
