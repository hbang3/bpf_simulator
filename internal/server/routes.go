package server

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/justinas/alice"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func (s *Server) RegisterRoutes() http.Handler {
	r := httprouter.New()
	r.ServeFiles("/static/*filepath", http.Dir("static"))
	r.HandlerFunc(http.MethodGet, "/example", s.example)
	r.HandlerFunc(http.MethodPost, "/process", s.process)
	r.HandlerFunc(http.MethodGet, "/about", s.about)
	r.HandlerFunc(http.MethodGet, "/jumptarget-mismatch", s.jumptargetMismatch)
	r.HandlerFunc(http.MethodGet, "/healthz", s.healtz)
	r.HandlerFunc(http.MethodGet, "/started", s.started)
	r.Handler(http.MethodGet, "/metrics", promhttp.Handler())
	r.HandlerFunc(http.MethodGet, "/", s.home)

	std := alice.New(s.recoverPanic, s.logRequest, commonHeader)
	return std.Then(r)
}
