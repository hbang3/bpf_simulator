package server

import (
	"bpf-simulator/pkg/bpfsim"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"text/template"
	"time"

	_ "github.com/joho/godotenv/autoload"
	cmap "github.com/orcaman/concurrent-map/v2"
	"go.uber.org/zap"
)

type Server struct {
	port          int
	logger        *zap.SugaredLogger
	templateCache map[string]*template.Template
	cache         cmap.ConcurrentMap[string, *bpfsim.CaperOut]

	HttpServer  *http.Server
	StartedTime time.Time
}

func NewServer() *Server {
	logger, err := zap.NewProduction()
	if err != nil {
		panic("error instantiating logger")
	}
	defer logger.Sync()
	sugar := logger.Sugar()

	tCache, err := newTemplateCache()
	if err != nil {
		sugar.Fatal("cannot parse templates", err)
	}

	caperCache := cmap.New[*bpfsim.CaperOut]()

	port, _ := strconv.Atoi(os.Getenv("PORT"))
	newServer := &Server{
		port:          port,
		logger:        sugar,
		templateCache: tCache,
		cache:         caperCache,
	}

	// Declare Server config
	httpServer := &http.Server{
		Addr:         fmt.Sprintf(":%d", newServer.port),
		Handler:      newServer.RegisterRoutes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	newServer.HttpServer = httpServer
	return newServer
}
