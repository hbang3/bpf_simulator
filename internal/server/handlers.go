package server

import (
	"bpf-simulator/pkg/bpfsim"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type ProcessForm struct {
	PcapExpression string
	Optimize       bool
	NotExpand      bool
	FileName       string
	MaxRec         int
	FieldErrors    map[string]string
	PktIdx         int
	PktNum         int
}

func (s *Server) home(w http.ResponseWriter, r *http.Request) {
	s.render(w, r, http.StatusOK, "home.tmpl.html", templateData{
		Form: "",
	})
}

func (s *Server) started(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	data := (time.Since(s.StartedTime)).String()
	w.Write([]byte(data))
}

func (s *Server) healtz(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	w.Write([]byte("ok"))
}

func (s *Server) process(w http.ResponseWriter, r *http.Request) {
	promRequestCount.Inc()
	startTime := time.Now()

	err := r.ParseMultipartForm(MAX_PCAP_FILE_SIZE)
	if err != nil {
		s.logger.Error("MultipartForm parsing error", err)
		s.serverError(w, r, err)
		return
	}
	err = r.ParseForm()
	if err != nil {
		s.logger.Error("Parsing Form error", err)
		s.serverError(w, r, err)
		return
	}

	m := r.PostForm.Get("max_rec")
	maxRecVal, err := strconv.Atoi(m)
	if err != nil && m == "" {
		maxRecVal = 1
	} else if err != nil {
		s.logger.Error("Error parsing max_rec value. Max_rec must be between 0 and 8", err)
		s.serverError(w, r, err)
		return
	}

	maxPkts := r.PostForm.Get("pkt-num")
	maxPktsNum, err := strconv.Atoi(maxPkts)
	if err != nil && m == "" {
		maxPktsNum = 10
	} else if err != nil {
		s.logger.Error("Error parsing max_pkt value. Max Pkt must be less than 50", err)
		s.serverError(w, r, err)
		return
	}

	pktIndex := r.PostForm.Get("pkt-idx")
	pktIdx, err := strconv.Atoi(pktIndex)
	if err != nil && m == "" {
		pktIdx = 0
	} else if err != nil {
		s.logger.Error("invalid starting index of pkt", err)
		s.serverError(w, r, err)
		return
	}

	file, handler, err := r.FormFile("pcap-file")
	if err != nil {
		s.logger.Error(err)
		s.serverError(w, r, err)
		return
	}
	defer file.Close()

	pcap_file_fmt, err := regexp.Compile(".*(.pcap|.pcapng|.cap)")
	if err != nil {
		s.serverError(w, r, err)
		return
	}
	if !pcap_file_fmt.MatchString(handler.Filename) {
		s.serverError(w, r, err)
		return
	}

	f, err := os.OpenFile("./pcap_files/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		s.serverError(w, r, err)
		return
	}
	defer f.Close()

	_, err = io.Copy(f, file)
	if err != nil {
		s.serverError(w, r, err)
		return
	}

	form := ProcessForm{
		PcapExpression: r.PostForm.Get("pcap-expression"),
		Optimize:       r.PostForm.Get("optimized") == "on",
		NotExpand:      r.PostForm.Get("not_expand") == "on",
		FileName:       handler.Filename,
		MaxRec:         maxRecVal,
		PktNum:         maxPktsNum,
		PktIdx:         pktIdx,
	}

	if strings.TrimSpace(form.PcapExpression) == "" {
		form.FieldErrors["pcap-expression"] = "This field cannot be blank"
	}
	if form.MaxRec < 0 || form.MaxRec > 8 {
		form.FieldErrors["max-rec"] = "max-rec must be bewteen 0 and 8"
	}
	if form.FileName == "" {
		form.FieldErrors["pcap-file"] = "File must be given"
	}

	// TODO create error tmpl
	if len(form.FieldErrors) > 0 {
		s.render(w, r, http.StatusUnprocessableEntity, "error.tmpl.html", templateData{})
	}

	cArgs := bpfsim.CaperArgs{
		PcapExp:   form.PcapExpression,
		Quite:     true,
		Optimized: form.Optimize,
		Expand:    !form.NotExpand,
		MaxRec:    form.MaxRec,
	}
	s.logger.Info("get caper arguments ", cArgs)

	var caperOut *bpfsim.CaperOut
	if cout, ok := s.cache.Get(form.PcapExpression); ok {
		s.logger.Info("cache hit!")
		caperOut = cout
	} else {
		s.logger.Info("cache miss!")
		caperOut, err = bpfsim.GetCaperOut(&cArgs)
		if err != nil {
			s.logger.Error("cannot get output from caper")
		}
	}

	fileName := fmt.Sprintf("./pcap_files/%s", form.FileName)
	SymbolicBPF, Results, err := bpfsim.RunBpfSim(fileName, form.PktIdx, form.PktNum, caperOut)
	if err != nil {
		s.logger.Error(err)
		s.serverError(w, r, err)
	}
	s.logger.Info("bpf out", SymbolicBPF)

	if len(Results) == 0 {
		// TODO:: Add redirect to no-packet
	}

	s.render(w, r, http.StatusOK, "process.tmpl.html", templateData{
		Results:     Results,
		SymbolicBPF: SymbolicBPF,
		ProcessForm: form,
	})

	timeTaken := time.Now().Sub(startTime).Milliseconds()
	s.logger.Infow("time taken to process", "t", timeTaken)
	promRequestProcessTimeHist.Observe(float64(timeTaken))
}

func (s *Server) example(w http.ResponseWriter, r *http.Request) {
	f, err := os.OpenFile("./pcap_files/chrome-cloudflare-quic.pcapng", os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		s.serverError(w, r, err)
		return
	}
	defer f.Close()

	form := ProcessForm{
		PcapExpression: "tcp or udp",
		Optimize:       true,
		NotExpand:      false,
		FileName:       "chrome-cloudflare-quic.pcapng",
		MaxRec:         0,
		PktNum:         10,
		PktIdx:         45,
	}

	cArgs := bpfsim.CaperArgs{
		PcapExp:   form.PcapExpression,
		Quite:     true,
		Optimized: form.Optimize,
		Expand:    !form.NotExpand,
		MaxRec:    form.MaxRec,
	}
	s.logger.Info("get caper arguments ", cArgs)

	var caperOut *bpfsim.CaperOut
	if cout, ok := s.cache.Get(form.PcapExpression); ok {
		s.logger.Info("cache hit!")
		caperOut = cout
	} else {
		s.logger.Info("cache miss!")
		caperOut, err = bpfsim.GetCaperOut(&cArgs)
		if err != nil {
			s.logger.Error("cannot get output from caper", err)
			s.serverError(w, r, err)
		}
		s.cache.Set(form.PcapExpression, caperOut)
	}
	SymbolicBPF, Results, err := bpfsim.RunBpfSim("./pcap_files/chrome-cloudflare-quic.pcapng", form.PktIdx, form.PktNum, caperOut)
	if err != nil {
		s.logger.Error(err)
		s.serverError(w, r, err)
	}
	s.logger.Info("bpf out", SymbolicBPF)

	s.render(w, r, http.StatusOK, "process.tmpl.html", templateData{
		Results:     Results,
		SymbolicBPF: SymbolicBPF,
		ProcessForm: form,
	})
}

func (s *Server) about(w http.ResponseWriter, r *http.Request) {
	s.render(w, r, http.StatusOK, "about.tmpl.html", templateData{
		Form: "",
	})
}

func (s *Server) jumptargetMismatch(w http.ResponseWriter, r *http.Request) {
	s.render(w, r, http.StatusOK, "jumptarget-mismatch.tmpl.html", templateData{
		Form: "",
	})
}
