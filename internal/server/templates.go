package server

import (
	"path/filepath"
	"text/template"
	"time"
)

type templateData struct {
	Form        any
	Results     any
	SymbolicBPF string
	ProcessForm ProcessForm
}

func humanDate(t time.Time) string {
	return t.Format("02 Jan 2006 at 15:04")
}

var functions = template.FuncMap{
	"humanDate": humanDate,
}

func newTemplateCache() (map[string]*template.Template, error) {
	cache := map[string]*template.Template{}

	pages, err := filepath.Glob("./ui/html/pages/*.tmpl.html")
	if err != nil {
		return nil, err
	}

	for _, page := range pages {
		name := filepath.Base(page)

		ts, err := template.ParseFiles("./ui/html/pages/base.tmpl.html")
		ts = ts.Funcs(template.FuncMap{"mod": func(i, j int) int { return i % j }})
		ts = ts.Funcs(template.FuncMap{"arith_eq": func(i, j int) bool { return i == j }})
		ts = ts.Funcs(template.FuncMap{"add": func(i, j int) int { return i + j }})
		if err != nil {
			return nil, err
		}

		ts, err = ts.ParseFiles(page)
		if err != nil {
			return nil, err
		}

		cache[name] = ts
	}
	return cache, nil
}
