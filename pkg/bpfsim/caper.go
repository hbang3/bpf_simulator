package bpfsim

import (
	"fmt"
	"os/exec"
	"sync"

	"github.com/dominikbraun/graph"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
)

type CaperArgs struct {
	PcapExp   string
	Quite     bool
	Optimized bool
	Expand    bool
	MaxRec    int
}

type CaperOut struct {
	SymbolicBPF string
	SockFilters map[int]SockFilter
	BpfBlocks   map[int][]string
}

type bpfSimOut struct {
	Edges     []Edge
	RawPacket string
	Base64Img string
}

const caperBinDir = "caper/caper.byte"

//const caperBinDir = "usr/bin/caper"

func GetCaperOut(cArgs *CaperArgs) (*CaperOut, error) {
	symbolicBPF, err := caperExec(cArgs)
	if err != nil {
		return nil, err
	}

	sockFilters, bpfBlocks, err := ParseBPF(string(symbolicBPF))
	if err != nil {
		return nil, err
	}

	cOut := &CaperOut{
		SymbolicBPF: symbolicBPF,
		SockFilters: sockFilters,
		BpfBlocks:   bpfBlocks,
	}
	return cOut, nil
}

func RunBpfSim(pcapFile string, pktIdx int, pktNum int, cOut *CaperOut) (string, []*bpfSimOut, error) {
	g, err := GenerateSkeletonGraph(cOut)
	if err != nil {
		return "", nil, err
	}

	// 2. process raw packet
	var wg sync.WaitGroup
	bpfSimChan := make(chan *bpfSimOut, pktNum)

	handle, err := pcap.OpenOffline(pcapFile)
	if err != nil {
		return "", nil, err
	}
	defer handle.Close()

	i := 0
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	for packet := range packetSource.Packets() {
		if i < pktIdx {
			i++
			continue
		}
		if i >= pktIdx && i < (pktIdx+pktNum) {
			wg.Add(1)
			// process each packet based on the output of caper
			go cOut.processPacket(packet.Data(), bpfSimChan, &wg)
			i++
		}
	}

	wg.Wait()
	close(bpfSimChan)

	graphResultChan := make(chan *bpfSimOut, pktNum)
	// 3. generate Graph Image
	for bOut := range bpfSimChan {
		if bOut == nil {
			continue
		}
		wg.Add(1)
		// generate graph based on the bpfSimulation
		go bOut.generateGraph(i, g, graphResultChan, &wg)
	}

	wg.Wait()
	close(graphResultChan)

	finalOut := []*bpfSimOut{}
	for gOut := range graphResultChan {
		finalOut = append(finalOut, gOut)
	}
	return cOut.SymbolicBPF, finalOut, nil
}

func caperExec(cargs *CaperArgs) (string, error) {
	args := cargs.toFlags()

	cmd := exec.Command(caperBinDir, args...)
	symbolicBPF, err := cmd.Output()
	if err != nil {
		return "", err
	}

	return string(symbolicBPF), nil
}

func (c *CaperArgs) toFlags() []string {
	args := []string{"-q"}
	if c.Optimized {
		args = append(args, "-BPF_optimized")
	} else {
		args = append(args, "-BPF")
	}

	if !c.Expand {
		args = append(args, "-not_expand")
	}

	if c.MaxRec < 0 || c.MaxRec > 50 {
		c.MaxRec = 1
	}
	args = append(args, "-max_rec", fmt.Sprint(c.MaxRec))
	args = append(args, "-e", c.PcapExp)
	return args
}

func (c *CaperOut) processPacket(pkt []byte, pcapResultChan chan<- *bpfSimOut, wg *sync.WaitGroup) {
	defer wg.Done()
	pOut, _ := c.ProcessPacket(pkt)
	// if err != nil {
	// 	log.Fatal(err)
	// 	pcapResultChan <- &pcapOutput{}
	// }
	pcapResultChan <- pOut
}

func (b *bpfSimOut) generateGraph(idx int, g graph.Graph[int, int], graphResultChan chan<- *bpfSimOut, wg *sync.WaitGroup) {
	defer wg.Done()
	newPout, _ := b.GenerateBPFSimulationResult(idx, g)
	// if err != nil {
	// 	graphResultChan <- &pcapOutput{}
	// }
	graphResultChan <- newPout
}
