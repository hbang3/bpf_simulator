package bpfsim

import (
	"encoding/base64"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/dominikbraun/graph"
	"github.com/dominikbraun/graph/draw"
)

func GenerateSkeletonGraph(cOut *CaperOut) (graph.Graph[int, int], error) {
	g := graph.New(graph.IntHash, graph.Directed())
	for key, elem := range cOut.BpfBlocks {
		err := g.AddVertex(key, graph.VertexAttribute("shape", "box"), graph.VertexAttribute("label", strings.Join(elem, "\\l")))
		if err != nil {
			return nil, err
		}
	}

	for key, elem := range cOut.SockFilters {
		if elem.jt == -1 && elem.jf == -1 {
			continue
		}
		err := g.AddEdge(key, elem.jt, graph.EdgeAttribute("color", "gray"), graph.EdgeAttribute("style", "dotted"))
		if err != nil {
			return nil, err
		}

		err = g.AddEdge(key, elem.jf, graph.EdgeAttribute("color", "gray"), graph.EdgeAttribute("style", "dotted"))
		if err != nil {
			return nil, err
		}
	}
	return g, nil
}

func edgeToLabel(edge Edge) string {
	if edge.cond {
		return fmt.Sprintf("T\\lA: 0x%X\\lX: 0x%X", edge.aReg, edge.xReg)
	} else {
		return fmt.Sprintf("F\\lA: 0x%X\\lX: 0x%X", edge.aReg, edge.xReg)
	}
}

func (bOut *bpfSimOut) GenerateBPFSimulationResult(idx int, skeletonGraph graph.Graph[int, int]) (*bpfSimOut, error) {
	new_graph, err := skeletonGraph.Clone()
	if err != nil {
		return nil, err
	}

	for _, edge := range bOut.Edges {
		newLabel := edgeToLabel(edge)
		err = new_graph.UpdateEdge(
			edge.from,
			edge.to,
			graph.EdgeAttribute("label", newLabel),
			graph.EdgeAttribute("style", "solid"),
			graph.EdgeAttribute("color", "green"),
			graph.EdgeAttribute("pensidth", "5"))
		if err != nil {
			return nil, err
		}
	}

	gvFileName := fmt.Sprintf("graph%d.gv", idx)
	gv_file, err := os.CreateTemp("", gvFileName)
	if err != nil {
		return nil, err
	}
	defer os.Remove(gv_file.Name())

	err = draw.DOT(new_graph, gv_file)
	if err != nil {
		return nil, err
	}

	img, err := exec.Command("dot", "-Tpng", gv_file.Name(), "-Gdpi=75", "-Gbgcolor=transparent").Output()
	if err != nil {
		return nil, err
	}

	bOut.Base64Img = base64.StdEncoding.EncodeToString(img)
	return bOut, nil
}
