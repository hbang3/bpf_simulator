package bpfsim

const MAX_SIMULATION = 10

const (
	ADDITION       = "addition"
	MULTIPLICATION = "multiplication"
	LEFTSHIFT      = "left_shift"
	RIGHTSHIFT     = "rihgt_shift"
)
const (
	LDH  = "ldh"
	LDB  = "ldb"
	LD   = "ld"
	LDX  = "ldx"
	LDXB = "ldxb"
	ADD  = "add"
	AND  = "and"
	MUL  = "mul"
	LSH  = "lsh"
	RSH  = "rsh"
	JEQ  = "jeq"
	JGT  = "jgt"
	JGE  = "jge"
	JSET = "jset"
	RET  = "ret"
	TAX  = "tax"
	TXA  = "txa"
	St   = "st"
	Stx  = "stx"
)

const (
	LIT    = "num"
	OFF    = "off"
	MEM    = "mem"
	X      = "x"
	X_ADD  = "x_add"
	IP_LEN = "4*([n]&0xf)"
)

type Opcode struct {
	inst string
	repr string
	val  uint32
}

type SockFilter struct {
	opcodes []Opcode
	jt      int
	jf      int
}

type Registers struct {
	aReg uint32
	xReg uint32
	mem  []uint32
}

type Edge struct {
	from int
	to   int
	cond bool
	aReg uint32
	xReg uint32
}
