package bpfsim

import (
	"errors"
	"fmt"
)

func (r *Registers) Ldb(pkt []uint8, opcode Opcode) error {
	switch opcode.repr {
	case OFF:
		if int(opcode.val) > len(pkt) {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.aReg = uint32(pkt[opcode.val])
		return nil
	case LIT:
		r.aReg = uint32(uint8(opcode.val))
		return nil
	case X_ADD:
		if int(r.xReg+opcode.val) >= len(pkt) {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.aReg = uint32(pkt[r.xReg+opcode.val])
		return nil
	case MEM:
		if opcode.val >= 16 {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.aReg = r.mem[opcode.val] & 0xFF
		return nil
	}
	return errors.New("LDB")
}

func (r *Registers) Ldh(pkt []uint8, opcode Opcode) error {
	switch opcode.repr {
	case OFF:
		if int(opcode.val+1) > len(pkt) {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.aReg = uint32(pkt[opcode.val])<<8 + uint32(pkt[opcode.val+1])
		return nil
	case LIT:
		r.aReg = uint32(uint16(opcode.val))
		return nil
	case X_ADD:
		if int(r.xReg+opcode.val+1) >= len(pkt) {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.aReg = uint32(pkt[r.xReg+opcode.val])<<8 + uint32(pkt[r.xReg+opcode.val+1])
		return nil
	case MEM:
		if opcode.val >= 16 {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.aReg = r.mem[opcode.val] & 0xFFFF
		return nil
	}
	return errors.New("LDH")
}

func (r *Registers) Ld(pkt []uint8, opcode Opcode) error {
	switch opcode.repr {
	case OFF:
		if int(opcode.val+3) > len(pkt) {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.aReg = uint32(pkt[opcode.val])<<24 + uint32(pkt[opcode.val+1])<<16 + uint32(pkt[opcode.val+2])<<8 + uint32(pkt[opcode.val+3])
		return nil
	case LIT:
		r.aReg = opcode.val
		return nil
	case X_ADD:
		if int(r.xReg+opcode.val+3) >= len(pkt) {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.aReg = uint32(pkt[r.xReg+opcode.val])<<24 + uint32(pkt[r.xReg+opcode.val+1])<<16 + uint32(pkt[r.xReg+opcode.val+2])<<8 + uint32(pkt[r.xReg+opcode.val+3])
		return nil
	case MEM:
		if opcode.val >= 16 {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.aReg = r.mem[opcode.val]
		return nil
	}
	return errors.New("LD")
}

func (r *Registers) Ldxb(pkt []uint8, opcode Opcode) error {
	switch opcode.repr {
	case OFF:
		if int(opcode.val) > len(pkt) {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.xReg = uint32(pkt[opcode.val])
		return nil
	case LIT:
		r.xReg = uint32(uint8(opcode.val))
		return nil
	case IP_LEN:
		if int(r.xReg+opcode.val) >= len(pkt) {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		temp := 4 * uint32((uint8(pkt[opcode.val]) & 0xf))
		r.xReg = uint32(temp)
		return nil
	case MEM:
		if opcode.val >= 16 {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.xReg = r.mem[opcode.val] & 0xFF
		return nil
	}
	return errors.New("LDXB")
}

func (r *Registers) Ldx(pkt []uint8, opcode Opcode) error {
	switch opcode.repr {
	case OFF:
		if int(opcode.val+3) > len(pkt) {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.xReg = uint32(pkt[opcode.val])<<24 + uint32(pkt[opcode.val+1])<<16 + uint32(pkt[opcode.val+2])<<8 + uint32(pkt[opcode.val+3])
		return nil
	case LIT:
		r.xReg = uint32(opcode.val)
		return nil
	case MEM:
		if opcode.val >= 16 {
			return errors.New(fmt.Sprint("index out of packet range", opcode))
		}
		r.xReg = r.mem[opcode.val]
		return nil
	}
	return errors.New("LDX")
}

func (r *Registers) Add(opcode Opcode) error {
	switch opcode.repr {
	case LIT:
		r.aReg += opcode.val
		return nil
	case X:
		r.aReg += r.xReg
		return nil
	}
	return errors.New("ADD")
}

func (r *Registers) And(opcode Opcode) error {
	switch opcode.repr {
	case LIT:
		r.aReg &= opcode.val
		return nil
	case X:
		r.aReg &= r.xReg
		return nil
	}
	return errors.New("AND")
}

func (r *Registers) Mul(opcode Opcode) error {
	switch opcode.repr {
	case LIT:
		r.aReg *= opcode.val
		return nil
	case X:
		r.aReg *= r.xReg
		return nil
	}
	return errors.New("ADD")
}

func (r *Registers) Rsh(opcode Opcode) error {
	switch opcode.repr {
	case LIT:
		r.aReg = r.aReg >> opcode.val
		return nil
	case X:
		r.aReg = r.aReg >> r.xReg
		return nil
	}
	return errors.New("Rsh")
}

func (r *Registers) Lsh(opcode Opcode) error {
	switch opcode.repr {
	case LIT:
		r.aReg = r.aReg << opcode.val
		return nil
	case X:
		r.aReg = r.aReg << r.xReg
		return nil
	}
	return errors.New("Rsh")
}

func (r *Registers) Tax() error {
	r.xReg = r.aReg
	return nil
}

func (r *Registers) Txa() error {
	r.aReg = r.xReg
	return nil
}

func (r *Registers) St(opcode Opcode) error {
	r.mem[opcode.val] = r.aReg
	return nil
}

func (r *Registers) Stx(opcode Opcode) error {
	r.mem[opcode.val] = r.xReg
	return nil
}
