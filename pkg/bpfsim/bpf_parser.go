package bpfsim

import (
	"bufio"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func lineToInt(s string) (int, error) {
	s = strings.Trim(s, " ")
	s = s[1:]
	return strconv.Atoi(s)
}

func hexToInt(s string) (int, error) {
	s = strings.Trim(s, " ")
	s = s[3:] // remove '#0x'
	ret, err := strconv.ParseInt(s, 16, 64)
	return int(ret), err
}

func offToInt(s string) (int, error) {
	s = strings.Trim(s, " ")
	s = s[1 : len(s)-1]
	return strconv.Atoi(s)
}

func XAddToInt(s string) (int, error) {
	s = strings.Trim(s, " ")
	s = s[1 : len(s)-1]
	s = strings.Split(s, "+")[1]
	return strconv.Atoi(strings.Trim(s, " "))
}

func IpLenToInt(s string) (int, error) {
	s = strings.Trim(s, " ")
	s = strings.Split(s, "[")[1]
	s = strings.Split(s, "]")[0]
	return strconv.Atoi(strings.Trim(s, " "))
}

func memToInt(s string) (int, error) {
	s = strings.Trim(s, " ")
	s = s[2 : len(s)-1]
	return strconv.Atoi(s)
}

func valToReprVal(value string) (string, int, error) {
	hex_regexp, err := regexp.Compile("#0x[0-9a-fA-f]+")
	if err != nil {
		return "", -1, fmt.Errorf("regex compile error")
	}
	lit_regexp, err := regexp.Compile("#[0-9]+")
	if err != nil {
		return "", -1, fmt.Errorf("regex compile error")
	}
	mem_regexp, err := regexp.Compile("M[[0-9]+]")
	if err != nil {
		return "", -1, fmt.Errorf("regex compile error")
	}
	off_regexp, err := regexp.Compile("[[0-9]+]")
	if err != nil {
		return "", -1, fmt.Errorf("regex compile error")
	}
	x_add_regexp, err := regexp.Compile(".*\\[.*x.*(\\+).*([0-9]+)\\]")
	if err != nil {
		return "", -1, fmt.Errorf("regex compile error")
	}
	ip_len_regexp, err := regexp.Compile("4 \\* \\(\\[([0-9]+)\\] & 0xf\\)")
	if err != nil {
		return "", -1, fmt.Errorf("regex compile error")
	}

	var val int
	if value == X {
		return X, 0, nil
	} else if hex_regexp.MatchString(value) {
		val, err = hexToInt(value)
		return LIT, val, err
	} else if lit_regexp.MatchString(value) {
		val, err = lineToInt(value)
		return LIT, val, err
	} else if mem_regexp.MatchString(value) {
		val, err = memToInt(value)
		return MEM, val, err
	} else if ip_len_regexp.MatchString(value) {
		val, err = IpLenToInt(value)
		return IP_LEN, val, err
	} else if x_add_regexp.MatchString(value) {
		val, err = XAddToInt(value)
		return X_ADD, val, err
	} else if off_regexp.MatchString(value) {
		val, err = offToInt(value)
		return OFF, val, err
	}
	return "", -1, fmt.Errorf("no regex are matched: %s", value)
}

func parseOpcode(op string) (Opcode, error) {
	op = strings.Trim(op, " ")
	inst := strings.Split(op, " ")[0]
	repr, val, err := valToReprVal(strings.Join(strings.Split(op, " ")[1:], " "))
	if inst != TAX && inst != TXA && err != nil {
		return Opcode{}, err
	}

	return Opcode{
		inst: inst,
		repr: repr,
		val:  uint32(val),
	}, nil
}

func ParseBPF(symbolicBPF string) (map[int]SockFilter, map[int][]string, error) {
	sockFilters := make(map[int]SockFilter)
	bpfBlocks := make(map[int][]string)

	var opcodes []Opcode
	var blocks []string
	var err error
	lineNum := -1

	scanner := bufio.NewScanner(strings.NewReader(symbolicBPF))
	for scanner.Scan() {
		line := strings.Split(scanner.Text(), ":")
		if lineNum == -1 {
			lineNum, err = lineToInt(line[0])
		}
		if err != nil {
			return nil, nil, err
		}

		opcode_jt_jf := line[1]
		opcode, err := parseOpcode(strings.Split(opcode_jt_jf, ",")[0])
		if err != nil {
			return nil, nil, err
		}

		if opcode.inst == JEQ || opcode.inst == JGT || opcode.inst == JGE || opcode.inst == JSET {
			opcodes = append(opcodes, opcode)
			blocks = append(blocks, scanner.Text())
			jt, err := lineToInt(strings.Split(opcode_jt_jf, ",")[1])
			if err != nil {
				return nil, nil, err
			}
			jf, err := lineToInt(strings.Split(opcode_jt_jf, ",")[2])
			if err != nil {
				return nil, nil, err
			}
			new_sf := SockFilter{
				opcodes: opcodes,
				jt:      jt,
				jf:      jf,
			}
			sockFilters[lineNum] = new_sf
			bpfBlocks[lineNum] = blocks
			opcodes = nil
			blocks = nil
			lineNum = -1
		} else if opcode.inst == RET {
			opcodes = append(opcodes, opcode)
			blocks = append(blocks, scanner.Text())
			new_sf := SockFilter{
				opcodes: opcodes,
				jt:      -1,
				jf:      -1,
			}
			sockFilters[lineNum] = new_sf
			bpfBlocks[lineNum] = blocks
			opcodes = nil
			blocks = nil
			lineNum = -1
		} else {
			opcodes = append(opcodes, opcode)
			blocks = append(blocks, scanner.Text())
		}
	}
	return sockFilters, bpfBlocks, nil
}
