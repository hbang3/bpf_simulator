package bpfsim

import (
	"fmt"
)

func byteArrayToHexString(byteArray []byte) string {
	hexString := "      00 01 02 03 04 05 06 07 08 09\n"
	hexString += "===================================\n"
	hexString += "0000| "

	for i, b := range byteArray {
		if (i)%10 == 0 && i != 0 {
			if i >= 300 {
				hexString += "\n=============truncated============\n"
				break
			}
			hexString += fmt.Sprintf("\n%04d| ", i)
		}
		hexString += fmt.Sprintf("%02X ", b)
	}
	return hexString
}

// process raw packet data using output from caper
func (c *CaperOut) ProcessPacket(data []byte) (*bpfSimOut, error) {
	sf := c.SockFilters[0]
	var ret []Edge

	r := &Registers{
		aReg: 0,
		xReg: 0,
		mem:  make([]uint32, 16),
	}

	var err error
	vertex := 0
	for sf.jt != -1 && sf.jf != -1 {
		for _, opcode := range sf.opcodes {
			switch opcode.inst {
			case LD:
				err = r.Ld(data, opcode)
			case LDX:
				err = r.Ldx(data, opcode)
			case LDH:
				err = r.Ldh(data, opcode)
			case LDB:
				err = r.Ldb(data, opcode)
			case LDXB:
				err = r.Ldxb(data, opcode)
			case ADD:
				err = r.Add(opcode)
			case AND:
				err = r.And(opcode)
			case MUL:
				err = r.Mul(opcode)
			case LSH:
				err = r.Lsh(opcode)
			case RSH:
				err = r.Rsh(opcode)
			case TAX:
				err = r.Tax()
			case TXA:
				err = r.Txa()
			case St:
				err = r.St(opcode)
			case Stx:
				err = r.Stx(opcode)
			case JSET:
				if (r.aReg & opcode.val) != 0 {
					newEdge := Edge{
						from: vertex,
						to:   sf.jt,
						cond: true,
						aReg: r.aReg,
						xReg: r.xReg,
					}
					ret = append(ret, newEdge)
					vertex = sf.jt
					sf = c.SockFilters[sf.jt]
				} else {
					newEdge := Edge{
						from: vertex,
						to:   sf.jf,
						cond: false,
						aReg: r.aReg,
						xReg: r.xReg,
					}
					ret = append(ret, newEdge)
					vertex = sf.jf
					sf = c.SockFilters[sf.jf]
				}
			case JEQ:
				if r.aReg == opcode.val {
					newEdge := Edge{
						from: vertex,
						to:   sf.jt,
						cond: true,
						aReg: r.aReg,
						xReg: r.xReg,
					}
					ret = append(ret, newEdge)
					vertex = sf.jt
					sf = c.SockFilters[sf.jt]
				} else {
					newEdge := Edge{
						from: vertex,
						to:   sf.jf,
						cond: false,
						aReg: r.aReg,
						xReg: r.xReg,
					}
					ret = append(ret, newEdge)
					vertex = sf.jf
					sf = c.SockFilters[sf.jf]
				}
			case JGT:
				if r.aReg > opcode.val {
					newEdge := Edge{
						from: vertex,
						to:   sf.jt,
						cond: true,
						aReg: r.aReg,
						xReg: r.xReg,
					}
					ret = append(ret, newEdge)
					vertex = sf.jt
					sf = c.SockFilters[sf.jt]
				} else {
					newEdge := Edge{
						from: vertex,
						to:   sf.jf,
						cond: false,
						aReg: r.aReg,
						xReg: r.xReg,
					}
					ret = append(ret, newEdge)
					vertex = sf.jf
					sf = c.SockFilters[sf.jf]
				}
			case JGE:
				if r.aReg >= opcode.val {
					newEdge := Edge{
						from: vertex,
						to:   sf.jt,
						cond: true,
						aReg: r.aReg,
						xReg: r.xReg,
					}
					ret = append(ret, newEdge)
					vertex = sf.jt
					sf = c.SockFilters[sf.jt]
				} else {
					newEdge := Edge{
						from: vertex,
						to:   sf.jf,
						cond: false,
						aReg: r.aReg,
						xReg: r.xReg,
					}
					ret = append(ret, newEdge)
					vertex = sf.jf
					sf = c.SockFilters[sf.jf]
				}
			}
		}
		if err != nil {
			return nil, err
		}
	}
	out := &bpfSimOut{
		Edges:     ret,
		RawPacket: byteArrayToHexString(data),
		Base64Img: "",
	}
	return out, nil
}
