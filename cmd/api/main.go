package main

import (
	"bpf-simulator/internal/server"
	"fmt"
	"time"
)

func main() {
	srv := server.NewServer()
	err := srv.HttpServer.ListenAndServe()
	srv.StartedTime = time.Now()
	if err != nil {
		panic(fmt.Sprintf("cannot start server: %s", err))
	}
}
