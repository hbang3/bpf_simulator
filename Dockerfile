FROM --platform=linux/amd64 ocaml/opam:debian-ocaml-4.08-flambda
#FROM ocaml/opam:debian-ocaml-4.08-flambda

RUN sudo apt-get update \
 && sudo apt-get install -y m4 \
 && sudo apt-get install libpcap-dev wget -y
RUN opam init -y
RUN opam install -y ocamlfind \
 && opam install -y ocamlbuild \
 && opam install -y menhir


# install go
RUN wget https://go.dev/dl/go1.23.4.linux-amd64.tar.gz
RUN sudo tar -C /usr/local -xzf go1.23.4.linux-amd64.tar.gz
RUN export PATH=$PATH:/usr/local/go/bin

WORKDIR $HOME
RUN git clone https://gitlab.com/hbang3/bpf_simulator.git

WORKDIR $HOME/bpf_simulator
RUN git clone https://gitlab.com/niksu/caper.git
RUN cd caper && ./build.sh
ENV PORT 8080
RUN /usr/local/go/bin/go mod tidy

CMD ["/usr/local/go/bin/go", "run", "cmd/api/main.go"]
